package com.chapur.bitacora.demo.entity;

import java.sql.Date;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.*;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;

@Entity
@Table(name = "EMPLOYEES")
//@JsonIdentityInfo(generator = ObjectIdGenerators.PropertyGenerator.class,  property = "EMPLOYEE_ID")
public class Employee {
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "employee_sequence")
	@SequenceGenerator(name = "employee_sequence", sequenceName = "EMPLOYEE_SEQ")
	@Column(name = "EMPLOYEE_ID")
	private Integer employeeId;
	
	@Column(name = "FIRST_NAME")
	private String firstName;
	
	@Column(name = "LAST_NAME")
	private String lastName;
	
	@Column(name = "EMAIL")
	private String email;
	
	@Column(name = "PHONE_NUMBER")
	private String phoneNumber;
	
	@Column(name = "HIRE_DATE")
	private Date hireDate;
	
	@Column(name = "JOB_ID")
	private String jobId;
	
	@Column(name = "SALARY")
	private float salary;
	
	@Column(name = "COMMISSION_PCT")
	private Float commissionPct;
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="DEPARTMENT_ID")
	//@JsonBackReference
	@JsonManagedReference
	private Department department;
	
//	@ManyToOne(fetch = FetchType.LAZY)
//	@JoinColumn(name = "MANAGER_ID")
//	@JsonManagedReference
//	//@JsonIgnore
//	private Employee manager;
	
//	@OneToMany(mappedBy="manager", fetch = FetchType.LAZY, cascade = CascadeType.ALL, orphanRemoval=true)
//	@JsonIgnore
//	private Set<Employee> empleados = new HashSet<Employee>();

	public Integer getEmployeeId() {
		return employeeId;
	}

	public void setEmployeeId(int employeeId) {
		this.employeeId = employeeId;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getPhoneNumber() {
		return phoneNumber;
	}

	public void setPhoneNumber(String phoneNumber) {
		this.phoneNumber = phoneNumber;
	}

	public Date getHireDate() {
		return hireDate;
	}

	public void setHireDate(Date hireDate) {
		this.hireDate = hireDate;
	}

	public String getJobId() {
		return jobId;
	}

	public void setJobId(String jobId) {
		this.jobId = jobId;
	}

	public float getSalary() {
		return salary;
	}

	public void setSalary(float salary) {
		this.salary = salary;
	}

	public Float getCommissionPct() {
		return commissionPct;
	}

	public void setCommissionPct(Float commissionPct) {
		this.commissionPct = commissionPct;
	}

	public Department getDepartment() {
		return department;
	}

	public void setDepartment(Department department) {
		this.department = department;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((department == null) ? 0 : department.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) return true;
        if (!(obj instanceof Employee )) return false;
        return this.employeeId != null && this.employeeId.equals(((Employee) obj).employeeId);
	}

//	public Employee getManager() {
//		return manager;
//	}
//
//	public void setManager(Employee manager) {
//		this.manager = manager;
//	}

//	public Set<Employee> getEmpleados() {
//		return empleados;
//	}
//
//	public void setEmpleados(Set<Employee> empleados) {
//		this.empleados = empleados;
//	}
	
	

}
