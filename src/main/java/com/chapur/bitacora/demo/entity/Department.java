package com.chapur.bitacora.demo.entity;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonBackReference;

@Entity
@Table(name = "DEPARTMENTS")
public class Department {
	
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "department_sequence")
	@SequenceGenerator(name = "department_sequence", sequenceName = "DEPARTMENTS_SEQ")
	@Column(name = "DEPARTMENT_ID")
	private int departmentId;
	
	@Column(name = "DEPARTMENT_NAME")
	private String departmentName;
	
	@Column(name = "LOCATION_ID")
	private int locationId;
	
	@OneToMany(mappedBy = "department", fetch = FetchType.LAZY, cascade = CascadeType.ALL, orphanRemoval=true)
	@JsonBackReference
    private List<Employee> employees = new ArrayList<>();
	
//	@Column(name = "MANAGER_ID")
//	private Integer managerId;
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "MANAGER_ID")
	private Employee manager;
	
	public Department() {
		
	}

	public int getDepartmentId() {
		return departmentId;
	}

	public void setDepartmentId(int departmentId) {
		this.departmentId = departmentId;
	}

	public String getDepartmentName() {
		return departmentName;
	}

	public void setDepartmentName(String departmentName) {
		this.departmentName = departmentName;
	}

	public int getLocationId() {
		return locationId;
	}

	public void setLocationId(int locationId) {
		this.locationId = locationId;
	}

//	public Integer getManagerId() {
//		return managerId;
//	}
//
//	public void setManagerId(Integer managerId) {
//		this.managerId = managerId;
//	}
	
	public void addEmployee(Employee employee) {
		this.employees.add(employee);
		employee.setDepartment(this);
	}
	
	public void removeEmployee(Employee employee) {
		this.employees.remove(employee);
		employee.setDepartment(null);
	}
	
	
	public Employee getManager() {
		return manager;
	}

	public void setManager(Employee manager) {
		this.manager = manager;
	}

	@Override
	public String toString() {
		return "Department [departmentId=" + departmentId + ", departmentName=" + departmentName + ", locationId="
				+ locationId + ", employees=" + employees + ", manager=" + manager + "]";
	}
	
	

}
