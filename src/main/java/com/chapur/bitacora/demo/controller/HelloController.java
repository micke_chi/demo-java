package com.chapur.bitacora.demo.controller;

import org.springframework.stereotype.Controller;
//import org.springframework.web.bind.annotation.RestController;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class HelloController {
	
	@Value("${proyecto.nombre}")
	private String nombre;
	
	@RequestMapping("/")
	public String index() {
		return "web/home";
		//return "Nombre del proyecto" + this.nombre;
	}
	
}
