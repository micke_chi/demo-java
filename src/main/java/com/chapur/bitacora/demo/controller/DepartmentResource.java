package com.chapur.bitacora.demo.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import com.chapur.bitacora.demo.entity.Department;
import com.chapur.bitacora.demo.entity.Employee;
import com.chapur.bitacora.demo.service.HumanResourceService;

@RestController
public class DepartmentResource {

	@Autowired
	private HumanResourceService humanResourcesService;

	@GetMapping("api/department")
	public List<Department> allDepartments() {

		List<Department> deptos = this.humanResourcesService.getAllDepartments();
		for (Department depto : deptos) {
			System.out.println("Depto: " + depto.getDepartmentName());
		}
		
		//Department firstDepto = deptos.get(0);
		//String nameDepto = firstDepto.getDepartmentName();
		return deptos;
	}

	@GetMapping("api/employee")
	public List<Employee> allEmployees() {

		List<Employee> employees = this.humanResourcesService.getAllEmployees();
		for (Employee empl : employees) {
			Department deptoEmp = empl.getDepartment();
			System.out.println("Depto: " + deptoEmp.getDepartmentName());
		}
		return employees;
	}

	@GetMapping("api/department/{departmentId}/employees}")
	public List<String> employeesInDepartment(@PathVariable int departmentId) {
		return null;
		// return this.humanResourcesService.getAllDepartmentEmployees(departmentId);

	}

}
