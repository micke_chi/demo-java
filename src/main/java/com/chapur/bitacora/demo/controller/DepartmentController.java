package com.chapur.bitacora.demo.controller;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;

import com.chapur.bitacora.demo.entity.Department;
import com.chapur.bitacora.demo.service.DepartmentService;

@Controller
//@RequestMapping("department")
public class DepartmentController {

	@Autowired
	private DepartmentService departmentService;

	@GetMapping("/department")
	public String index(Model model) {
		List<Department> allDepartments = this.departmentService.getAll();
		model.addAttribute("departments", allDepartments);
		//model.addAttribute("demo demo", "Hola mundo");
		return "web/departments";
	}

	@GetMapping("/department/create")
	public String create(Model model) {
		Department depto = new Department();
		model.addAttribute("depto", depto);
		return "web/department-create";
	}

	@PostMapping("/department")
	public String store(@Valid Department depto, BindingResult result, Model model) {
		if (result.hasErrors()) {
			return "web/department-create";
		}
		this.departmentService.addDepartment(depto);
		model.addAttribute("departments", this.departmentService.getAll());
		return "redirect:/department";
	}

	@GetMapping("/department/edit/{id}")
	public String edit(@PathVariable("id") int departmentId, Model model) {
		Department depto = this.departmentService.getById(departmentId);
		model.addAttribute("depto", depto);

		return "web/department-update";
	}

	@PutMapping("/department/{id}")
	public String update(@Valid Department depto, BindingResult result, Model model,
			@PathVariable("id") int departmentId) {
		this.departmentService.updateDepartment(departmentId, depto);
		model.addAttribute("departments", this.departmentService.getAll());
		return "redirect:/department";
	}

	@GetMapping("/department/delete/{id}")
	public String delete(@PathVariable("id") int departmentId, Model model) {

		this.departmentService.deleteDepartment(departmentId);
		model.addAttribute("departments", this.departmentService.getAll());
		
		return "redirect:/department";
		//return "web/departments";
	}
	
	
	@GetMapping("/example")
	public String example(Model model) {

		//this.departmentService.deleteDepartment(departmentId);
		model.addAttribute("departments", this.departmentService.getAll());

		System.out.println("pruebas reload");
		return "redirect:/department";
		//return "web/departments";
	}

	
}
