package com.chapur.bitacora.demo.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.chapur.bitacora.demo.entity.Department;
import com.chapur.bitacora.demo.entity.Employee;
import com.chapur.bitacora.demo.repository.DepartmentRepository;
import com.chapur.bitacora.demo.repository.EmployeeRepository;

@Service
public class HumanResourcesServiceImp implements HumanResourceService {

	@Autowired
	private EmployeeRepository employeeRepository;

	@Autowired
	private DepartmentRepository departmentRepository;


//	@Override
//	public List<String> getAllDepartmentEmployees(int departmentId) {
//
//		List<String> result = new ArrayList<String>();
//		List<Employee> employees = this.employeeRepository.findByDepartmentId(this.departmentId);
//		for (Employee employee : employees) {
//			result.add(employee.getFirstName());
//		}
//		// TODO Auto-generated method stub
//		return result;
//	}

//	@Override
//	public void addDepartmentEmployee(int departmentId, Employee employee) {
//		// TODO Auto-generated method stub
//		Department depto1 = this.departmentRepository.findByDepartmentId(this.departmentId);
//		employee.setDepartment(depto1);
//	}

	@Override
	public List<Department> getAllDepartments() {
		// TODO Auto-generated method stub
		List<Department> deptos = this.departmentRepository.findAll();

//		List<String> result = new ArrayList<String>();
//
//		for (Department depto : deptos) {
//			System.out.println(depto.getDepartmentName());
//		}

		return deptos;
	}

	@Override
	public List<Employee> getAllEmployees() {
		// TODO Auto-generated method stub
		List<Employee> employees = this.employeeRepository.findAll();

//		List<String> result = new ArrayList<String>();
//
//		for (Employee empl : employees) {
//			result.add(empl.getFirstName());
//		}

		return employees;
	}

}
