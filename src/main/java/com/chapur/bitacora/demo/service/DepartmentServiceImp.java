package com.chapur.bitacora.demo.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.chapur.bitacora.demo.entity.Department;
import com.chapur.bitacora.demo.repository.DepartmentRepository;

@Service
public class DepartmentServiceImp implements DepartmentService{
	
	@Autowired
	private DepartmentRepository departmentRepository;

	@Override
	public List<Department> getAll() {
		// TODO Auto-generated method stub
		List<Department> deptos = this.departmentRepository.findAll();
		return deptos;
	}

	@Override
	public Department getById(int departmentId) {
		// TODO Auto-generated method stub		
		return this.departmentRepository.findById(departmentId)
				.orElseThrow(()->new IllegalArgumentException("Invalid department id " + departmentId));
	}

	@Override
	public synchronized boolean addDepartment(Department depto) {
		// TODO Auto-generated method stub
		this.departmentRepository.save(depto);
		return true;
	}

	@Override
	public boolean updateDepartment(int departmentId, Department depto) {
		// TODO Auto-generated method stub
		Department deptoExist = this.departmentRepository.findById(departmentId)
				.orElseThrow(()->new IllegalArgumentException("Invalid department id " + departmentId));
		
		if(deptoExist instanceof Department) {
			depto.setDepartmentId(departmentId);
			this.departmentRepository.save(depto);
			return true;
		}
		
		return false;
	}

	@Override
	public boolean deleteDepartment(int departmentId) {
		// TODO Auto-generated method stub
		Department deptoExist = this.departmentRepository.findById(departmentId)
				.orElseThrow(()->new IllegalArgumentException("Invalid department id " + departmentId));
		
		if(deptoExist instanceof Department) {
			this.departmentRepository.delete(deptoExist);
			
			return true;
		}
		
		return false;
		
	}

}
