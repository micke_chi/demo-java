package com.chapur.bitacora.demo.service;

import java.util.List;

import com.chapur.bitacora.demo.entity.Department;
import com.chapur.bitacora.demo.entity.Employee;

public interface HumanResourceService {
	
	//public List<String> getAllDepartmentEmployees(int departmentId);
	
	//public void addDepartmentEmployee(int departmentId, Employee employee);
	
	public List<Department> getAllDepartments();
	
	public List<Employee> getAllEmployees();

}
