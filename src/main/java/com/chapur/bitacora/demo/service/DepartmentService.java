package com.chapur.bitacora.demo.service;

import java.util.List;

import com.chapur.bitacora.demo.entity.Department;

public interface DepartmentService {
	
	public List<Department> getAll();
	
	public Department getById(int departmentId);
	
	public boolean addDepartment(Department depto);
	
	public boolean deleteDepartment(int departmentId);
	
	public boolean updateDepartment(int departmentId, Department depto);

}
