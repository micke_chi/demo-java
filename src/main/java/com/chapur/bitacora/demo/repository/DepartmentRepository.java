package com.chapur.bitacora.demo.repository;

import java.util.List;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.chapur.bitacora.demo.entity.Department;

@Repository
public interface DepartmentRepository extends CrudRepository<Department, Integer> {
	//@Query
	//Department findByEmployees(int employeeId);
	
	List<Department> findAll();
	
	Department findByDepartmentId(int departmentId);

}
