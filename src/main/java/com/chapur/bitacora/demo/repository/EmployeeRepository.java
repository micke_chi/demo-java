package com.chapur.bitacora.demo.repository;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.chapur.bitacora.demo.entity.Employee;

@Repository
public interface EmployeeRepository extends CrudRepository<Employee, Integer> {
	//@Query
	//List<Employee> findByDepartmentId(int departmentId);
	
	List<Employee> findAll();
	
	
}
