import './scss/app.scss';

import Vue from 'vue';

//Plugins
import VueDraggableV1 from 'vue-draggable';
Vue.use(VueDraggableV1);

//Componentes
Vue.component('dragdrop', require('./components/DragDrop.vue').default);
Vue.component('columna-component', require('./components/ColumnaComponent.vue').default);

var app = new Vue({
    el: '#app',
});

